#!/usr/bin/env python

import sys
import time
import logging
from idf import constants as C


def printinfo(message):
    logging.info('%s' % message)
    print('%s' % message)


def logging_cfg():
    logging.basicConfig(
        format=C.LOG_FORMAT,
        filename=C.LOG_FILE,
        level=logging.DEBUG
        )
    logging.debug('Common init started:')


def common_init():
    logging_cfg()
    printinfo('Inside common init section')
