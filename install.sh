#!/bin/bash

set -x

repository_name="idf"
repository_url="https://oshchypko@bitbucket.org/oshchypko/${repository_name}.git"

cd /tmp

git clone ${repository_url}

cd ${repository_name}

apt-get update

apt-get install -y libssl-dev python-dev build-essential \
                    python-pip python-setuptools libffi-dev

easy_install -U pip
easy_install -U setuptools

pip install -U .

rm -rf /tmp/idf/
