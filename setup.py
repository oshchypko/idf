#!/usr/bin/env python

from setuptools import setup

setup(name='idf',
      version='0.1',
      description='Infrastructure Deployment Framework',
      url='http://github.com/idf',
      author='Volodymyr Oshchypko',
      author_email='idf@example.com',
      license='MIT',
      packages=['idf','idf.platform'],
      zip_safe=False
#      install_requires=["fabric"]
      )
